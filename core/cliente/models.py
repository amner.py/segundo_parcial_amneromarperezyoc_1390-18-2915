from django.db import models

class Cliente(models.Model):
    dni=models.CharField(verbose_name='DNI',db_column='DNI',primary_key=True,max_length=9,null=False,blank=False)
    nombre=models.CharField(verbose_name='Nombre',db_column='NOMBRE',max_length=20,null=False,blank=False)
    direccion=models.CharField(verbose_name='Dirección',db_column='DIRECCION',max_length=30,null=False,blank=False)
    fecha_nacimiento=models.DateField(verbose_name='Fecha de nacimiento',db_column='FECHA_NACIMIENTO',null=False,blank=False)

    def __str__(self):
        return f'{self.nombre} con DNI {self.dni}'

    class Meta():
        db_table='CLIENTE'
        verbose_name='Cliente'
        verbose_name_plural='Clientes'