import json
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.core.paginator import Paginator
from rest_framework import viewsets
from .serializers import ClienteSerializer
from .models import Cliente

class HomeView(TemplateView):
    template_name='home.html'
    
    def get(self, request):
        page=request.GET.get('page',1)
        data={}
        try:
            cs=Cliente.objects.all()

            paginator=Paginator(cs,5)
            clientes= paginator.page(page)
            data['paginator']=paginator
            data['entity']=clientes
            data['error']= False if len(clientes)>0 else True
        except:
            data['error']=True
        return render(request,self.template_name,data)
    
class ClienteViewSet(viewsets.ModelViewSet):
    queryset=Cliente.objects.all()
    serializer_class=ClienteSerializer