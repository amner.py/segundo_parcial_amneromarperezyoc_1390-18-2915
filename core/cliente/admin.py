from django.contrib import admin
from .models import Cliente

@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display=['dni','nombre','direccion','fecha_nacimiento']
    list_filter=[]
    list_editable=['direccion']
    list_per_page=15
    search_fields=['dni','nombre','direccion']